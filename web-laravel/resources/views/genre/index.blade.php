@extends('layout.master')
@section('title')
    Halaman List Genre
@endsection
@section('content')
    @auth
        <a href="/genre/create" class="btn btn-primary btn-sm mb-4">Tambah Genre</a>
    @endauth
    <table class="table">

        <thead class="thead-light">

            <tr>

                <th scope="col">#</th>

                <th scope="col">Nama Genre</th>

                <th scope="col">Actions</th>

            </tr>

        </thead>

        <tbody>

            @forelse ($genre as $key=>$value)
                <tr>
                    <td>{{ $key + 1 }}</th>
                    <td>{{ $value->nama }}</td>

                    <td>
                        <a href="/genre/{{ $value->id }}" class="btn btn-info">Detail</a>
                        <a href="/genre/{{ $value->id }}/edit" class="btn btn-primary">Edit</a>
                        <form action="/genre/{{ $value->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>
            @endforelse

        </tbody>

    </table>
    @endsection

