@extends('layout.master')

@section('title')
    Halaman Tambah Genre
@endsection

@section('content')
    <form action='/genre' method="POST">
        @csrf
        <div class="form-group">
            <label for="" class="form-label">nama</label>
            <input type="text" name="nama" id="nama" class="form-control" placeholder="" aria-describedby="helpId">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit">Submit</button>
        </div>
    </form>
@endsection
