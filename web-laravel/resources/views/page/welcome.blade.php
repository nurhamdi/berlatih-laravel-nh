@extends('layout.master')
@section('title')
    Halaman Selamat Datang
@endsection

@section('content')
    <h1>SELAMAT DATANG {{ $fName }} {{ $lName }}!</h1>
    <h3>Terima kasih telah bergabung ke Website kami. Media Belajar kita bersama!</h3>
    <p>
        Jenis Kelamin kamu adalah
        @if ($gender === '1')
            Laki-Laki
        @else
            Perempuan
        @endif
        <br>
        Kamu adalah warga negara {{ $nation }}<br>
        Sekilas mengenai diri kamu adalah {{ $bio }}
    </p>
@endsection
