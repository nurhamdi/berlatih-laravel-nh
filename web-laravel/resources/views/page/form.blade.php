@extends('layout.master')

@section('title')
    Halaman Form
@endsection

@section('content')
    
<h2>Buat Account Baru</h2>
<h3>Sign Up Form</h3>

<form action="/send" method="POST">
    @csrf
    <label>First Name</label><br>
    <input type="text" name="fName"><br>
    <label>Last Name</label><br>
    <input type="text" name="lName"><br>
    <br><label>Gender</label><br>
    <input type="radio" name="gender" value="1">Male<br>
    <input type="radio" name="gender" value="0">Female<br>
    <br><label>Nationality</label><br>
    <select name="nation">
        <option value="indonesia">Indonesia</option>
        <option value="aljazair">Aljazair</option>
        <option value="zambia">Zambia</option>
    </select>
    <br><br>
    <label>Language Spoken</label><br>
    <input type="checkbox" name="lang" value="IDN">Bahasa Indonesia<br>
    <input type="checkbox" name="lang" value="ENG">English<br>
    <input type="checkbox" name="lang" value="OTH">Other<br>
    <label>Bio</label><br>
    <textarea name="bio" rows="10" cols="30"></textarea><br>
    <input type="submit">
</form>
@endsection
