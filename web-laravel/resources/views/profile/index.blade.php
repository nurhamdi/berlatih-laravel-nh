@extends('layout.master')

@section('title')
    Halaman Update Profile
@endsection

@section('content')
    <form action="/profile/{{$detailProfile->id}}" method="POST">
        @csrf
        @method('put')

        <div class="form-group">
            <label for="" class="form-label">Nama</label>
            <input type="text" class="form-control" value="{{ $detailProfile->user->name }}"
                aria-describedby="helpId" disabled>
        </div>
        <div class="form-group">
            <label for="" class="form-label">Email</label>
            <input type="text" class="form-control" value="{{ $detailProfile->user->email }}"
                aria-describedby="helpId" disabled>
        </div>
        <div class="form-group">
            <label for="" class="form-label">umur</label>
            <input type="text" name="umur" id="umur" class="form-control" value="{{ $detailProfile->umur }}"
                aria-describedby="helpId">
        </div>
        @error('age')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="" class="form-label">bio</label>
            <textarea name="bio" id="bio" class="form-control" cols="10" rows="5">{{ $detailProfile->bio }}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="" class="form-label">Alamat</label>
            <textarea name="alamat" id="alamat" class="form-control" cols="10" rows="5">{{ $detailProfile->alamat }}</textarea>
        </div>
        @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit">Submit</button>
        </div>
    </form>
@endsection
