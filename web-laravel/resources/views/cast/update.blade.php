@extends('layout.master')

@section('title')
    Halaman update cast
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="" class="form-label">nama</label>
            <input type="text" name="nama" id="nama" class="form-control" value="{{$cast->nama}}" aria-describedby="helpId">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="" class="form-label">umur</label>
            <input type="text" name="umur" id="umur" class="form-control" value="{{$cast->umur}}"
                aria-describedby="helpId">
        </div>
        @error('age')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="" class="form-label">bio</label>
            <textarea name="bio" id="bio" class="form-control"  cols="10" rows="5">{{$cast->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit">Submit</button>
        </div>
    </form>
@endsection
