@extends('layout.master')

@section('title')
    Halaman List Cast
@endsection

@section('content')
    @auth
        <a href="/cast/create" class="btn btn-primary btn-sm mb-4">Tambah Cast</a>

    @endauth

    <div class="row">
        @forelse ($cast as $item)
            <div class="col-4">
                <div class="card">
                    {{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}
                    <div class="card-body">
                        <h3>{{ $item->nama }}</h3>
                        <p class="card-text">{{ $item->umur }} Years old</p>
                        <p class="card-text">{{ Str::limit($item->bio, 50) }}</p>
                        <a href="/cast/{{ $item->id }}" class="btn btn-secondary btn-block btn-sm">Cast Detail</a>
                        <div class="row my-2">
                            @auth
                                <div class="col">
                                    <a href="/cast/{{ $item->id }}/edit" class="btn btn-info btn-block btn-sm">edit</a>
                                </div>
                            @endauth
                            @auth
                                <div class="col">
                                    <form action="/cast/{{ $item->id }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <input type="submit" class="btn btn-danger btn-block btn-sm" value="delete">
                                    </form>
                                </div>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <h2>tidak ada data</h2>
        @endforelse
    </div>
@endsection
