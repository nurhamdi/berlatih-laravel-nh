@extends('layout.master')

@section('title')
    Halaman Tambah Cast
@endsection

@section('content')
    <form action='/cast' method="POST">
        @csrf
        <div class="form-group">
            <label for="" class="form-label">nama</label>
            <input type="text" name="nama" id="nama" class="form-control" placeholder="" aria-describedby="helpId">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="" class="form-label">umur</label>
            <input type="text" name="umur" id="umur" class="form-control" placeholder=""
                aria-describedby="helpId">
        </div>
        @error('age')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="" class="form-label">bio</label>
            <textarea name="bio" id="bio" class="form-control" cols="10" rows="5"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit">Submit</button>
        </div>
    </form>
@endsection
