@extends('layout.master')

@section('title')
    Halaman detail Cast
@endsection

@section('content')
    <h3>{{$cast->nama}}</h3>
    <p class="card-text">{{$cast->umur}}</p>
    <p class="card-text">{{($cast->bio)}}</p>
    <a href="/cast" class="btn btn-primary">Back to list</a>
@endsection
