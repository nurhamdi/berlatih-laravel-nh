<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src={{ asset('/template/dist/img/user2-160x160.jpg') }} class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            @auth
            <a href="#" class="d-block">{{Auth::user()->name}}</a>
            @endauth

            @guest
            <a href="#" class="d-block">Visitor</a>

            @endguest

        </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-sidebar">
                    <i class="fas fa-search fa-fw"></i>
                </button>
            </div>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
   with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="/" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Dashboard
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/" class="nav-link">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                        Halaman
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="/" class="nav-link">
                            <i class="nav-icon fas fa-home"></i>
                            <p>Home</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/genre" class="nav-link">
                            <i class="nav-icon fas fa-list"></i>
                            <p>Genre List</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/film" class="nav-link">
                            <i class="nav-icon fas fa-video"></i>
                            <p>Film List</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/cast" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>Cast</p>
                        </a>
                    </li>

                    @guest

                        <li class="nav-item bg-info">
                            <a href="/login" class="nav-link">
                                <p> login </p>
                            </a>
                        </li>

                    @endguest

                    @auth
                        <li class="nav-item">
                            <a href="/profile" class="nav-link">
                                <i class="nav-icon fas fa-user"></i>
                                <p>Profile</p>
                            </a>
                        </li>
                    @endauth

                    @auth

                        <li class="nav-item bg-danger">
                            <a class="nav-link" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>

                    @endauth
                </ul>
            </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
