@extends('layout.master')

@section('title')
    Halaman detail Film
@endsection

@section('content')
        <div class="row-fluid">
            <div class="span">
                <img style="float:left; width:25%" src="{{ asset('image/' . $film->poster) }}" />
                <div class="content-heading">
                    <h2>{{ $film->judul }}</h2>
                </div>
                <div class="content-heading">
                    <h4>{{ $film->tahun }}</h4>
                </div>
                <div class="content-heading">
                    <h4>{{ $film->genre->nama }}</h4>
                </div>
                <p>{{ $film->ringkasan }}</p>
                <div style="clear: left"></div>
                <a href="/film" class="btn btn-secondary btn-block btn-sm">Back to list</a>
            </div>
        </div>

        <hr>
        <h4>Daftar Kritik</h4>
        @forelse ($film->kritik as $item)
            <div class="media my-3 border p-3">
                <div class="media-body">
                    <hr>
                    <h5>{{$item->user->name}}</h5>
                    <hr>
                    <h6>Rating yang diberikan : {{$item->point}}</h6>
                    <p>{{$item->content}}
                </div>
            </div>
        @empty
            <h4>belum ada Kritik</h4>
        @endforelse
        <hr>

        <form action="/kritik/{{$film->id}}" method="POST">
        @csrf
        <div class="form-group">
            <label for="" class="form-label">Kritik</label>
            <textarea name="content" id="content" class="form-control" cols="10" rows="5"></textarea>
        </div>
        @error('kritik')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="" class="form-label">Nilai</label>
            <input type="number" min="1" max="10" name="point" id="point" class="form-control" placeholder="" aria-describedby="helpId">
        </div>
        @error('point')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <input type="submit" value="submit kritik">
    </form>
@endsection
