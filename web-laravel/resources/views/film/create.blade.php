@extends('layout.master')

@section('title')
    Halaman Tambah Film
@endsection

@section('content')
    <form action='/film' enctype="multipart/form-data" method="POST">
        @csrf
        <div class="form-group">
            <label for="" class="form-label">Judul</label>
            <input type="text" name="judul" id="judul" class="form-control" placeholder="" aria-describedby="helpId">
        </div>
        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="" class="form-label">Ringkasan</label>
            <textarea name="ringkasan" id="ringkasan" class="form-control" cols="10" rows="5"></textarea>
        </div>
        @error('ringkasan')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="" class="form-label">tahun</label>
            <input type="text" name="tahun" id="tahun" class="form-control" placeholder=""
                aria-describedby="helpId">
        </div>
        @error('tahun')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="" class="form-label">Poster</label>
            <input type="file" name="poster" id="poster" class="form-control" placeholder=""
                aria-describedby="helpId">
        </div>
        @error('poster')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="" class="form-label">Genre</label>
            <select name="genre_id" class="form-control">
                <option value="">Pilih Genre</option>
                @forelse ($genre as $item)
                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                @empty
                    <option value="">Data Genre Kosong</option>
                @endforelse
            </select>
        </div>
        @error('Genre')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div>
            <button type="submit">Submit</button>
        </div>
    </form>
@endsection
