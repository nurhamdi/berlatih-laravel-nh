@extends('layout.master')

@section('title')
    Halaman List Film
@endsection

@section('content')
    @auth
        <a href="/film/create" class="btn btn-primary btn-sm mb-4">Tambah Film</a>

    @endauth

    <div class="row">
        @forelse ($film as $item)
            <div class="col-4">
                <div class="card">
                    {{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}
                    <div class="card-body">
                        <h3>{{ $item->judul }}</h3>
                        <p class="card-text">Genre : {{ $item->genre->nama }}</p>
                        <p class="card-text">Year Released : {{ $item->tahun }}</p>
                        <img src="{{asset('image/' . $item->poster)}}" class="card-img-top">
                        <p class="card-text">{{ Str::limit($item->ringkasan, 50) }}</p>
                        <a href="/film/{{ $item->id }}" class="btn btn-secondary btn-block btn-sm">Film Detail</a>
                        <div class="row my-2">
                            @auth
                                <div class="col">
                                    <a href="/film/{{ $item->id }}/edit" class="btn btn-info btn-block btn-sm">edit</a>
                                </div>
                            @endauth
                            @auth
                                <div class="col">
                                    <form action="/film/{{ $item->id }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <input type="submit" class="btn btn-danger btn-block btn-sm" value="delete">
                                    </form>
                                </div>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <h2>tidak ada data</h2>
        @endforelse
    </div>
@endsection
