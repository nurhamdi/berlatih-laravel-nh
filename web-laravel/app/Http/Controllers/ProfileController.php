<?php

namespace App\Http\Controllers;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index(){
        $iduser = Auth::id();
        
        $detailProfile = Profile::where('user_id', $iduser)->first();

        return view('profile.index', ['detailProfile' => $detailProfile]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required',
        ]);

        $profile = Profile::find($id);

        $profile ->umur = $request['umur'];
        $profile ->bio = $request['bio'];
        $profile ->alamat = $request['alamat'];

        $profile->save();

        return redirect('/profile');
    }

}
