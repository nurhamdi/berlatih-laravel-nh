<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('page.form');
    }

    public function welcome()
    {
        return view('page.welcome');
    }

    public function send(Request $request)
    {
        // dd($request->all());
        $fName = $request['fName'];
        $lName = $request['lName'];
        $gender = $request['gender'];
        $nation = $request['nation'];
        $language = $request['lang'];
        $bio = $request['bio'];

        return view('page.welcome', ['fName' => $fName, 'lName' => $lName, 'gender' => $gender, 'nation' => $nation, 'language' => $language, 'bio' => $bio]);
    }

}