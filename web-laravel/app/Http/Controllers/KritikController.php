<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kritik;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;
class KritikController extends Controller
{

    public function index()
    {
        $userid = Auth::id();

        $detailProfile = Profile::where('user_id', $userid)->first();
        return view('profile.index', ['detailProfile'  => $detailProfile]);
    }


 
    public function add(Request $request, $id)
    {
        $request->validate([
            'content' => 'required',
            'point' => 'required',
        ]);

        $userid = Auth::id();

        $kritik = new Kritik;

        $kritik->user_id = $userid;
        $kritik->film_id = $id;
        $kritik->content = $request->content;
        $kritik->point = $request->point;

        $kritik->save();

        return redirect('/film/' . $id);
    }
}
