<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\KritikController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'main']);
Route::get('/data-tables', [IndexController::class, 'dataTable']);
Route::get('/register', [AuthController::class, 'form']);
// Route::get('/welcome', [AuthController::class, 'welcome']);
Route::post('/send', [AuthController::class, 'send']);
Route::post('/kritik/{film_id}', [KritikController::class, 'add']);

Route::middleware(['auth'])->group(function () {

    Route::resource('profile', ProfileController::class)->only(['index', 'update']);

});

//CRUD
Route::resource('cast', CastController::class);
Route::resource('genre', GenreController::class);
Route::resource('film', FilmController::class);




// Route::get('/master', function(){
//     return view('layout.master');
// });

// Route::get('/', function () {
//     return view('index');
// });

// Route::get('/', function () {
//     return view('page.index');
// });

// Route::get('/register', function () {
//     return view('page.form');
// });

// Route::get('/welcome', function () {
//     return view('page.welcome');
// });


Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
